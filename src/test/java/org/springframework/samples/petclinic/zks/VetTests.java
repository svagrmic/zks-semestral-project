package org.springframework.samples.petclinic.zks;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.vet.*;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
// Ensure that if the mysql profile is active we connect to the real database:
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class VetTests {

	@Autowired
	protected VetRepository vetRepository;

	@Autowired
	protected VetService vetService;

	@Autowired
	protected SpecialtyRepository specialtyRepository;

	@Autowired
	protected SpecialtyService specialtyService;

	@Test
	void shouldCreateNewVet() {
		String firstName = "Miguel";
		String lastName = "Rodriguez";

		Vet vet = new Vet();
		vet.setFirstName(firstName);
		vet.setLastName(lastName);
		vetRepository.save(vet);

		Vet createdVet = vetRepository.findById(vet.getId());

		assertThat(createdVet).isNotNull();
		assertThat(createdVet.getFirstName()).isEqualTo(firstName);
		assertThat(createdVet.getLastName()).isEqualTo(lastName);
		assertThat(createdVet.getNrOfSpecialties()).isEqualTo(0);
		assertThat(createdVet.getSpecialties()).isEmpty();

	}

	@Test
	void shouldCreateNewVetWithSpeciality() {
		String firstName = "Miguel";
		String lastName = "Rodriguez";

		Specialty specialty = specialtyRepository.findById(1);
		Vet vet = new Vet();
		vet.addSpecialty(specialty);
		vet.setFirstName(firstName);
		vet.setLastName(lastName);
		vetRepository.save(vet);

		Vet createdVet = vetRepository.findById(vet.getId());

		assertThat(createdVet).isNotNull();
		assertThat(createdVet.getFirstName()).isEqualTo(firstName);
		assertThat(createdVet.getLastName()).isEqualTo(lastName);
		assertThat(createdVet.getNrOfSpecialties()).isEqualTo(1);
		assertThat(createdVet.getSpecialties().get(0)).isEqualTo(specialty);
	}

	@Test
	void shouldUpdateVet() {
		String firstName = "Miguel";
		String lastName = "Rodriguez";

		Specialty specialty = specialtyRepository.findById(1);
		Vet vet = vetRepository.findById(1);
		assertThat(vet).isNotNull();
		assertThat(specialty).isNotNull();
		assertThat(vet.getSpecialties()).doesNotContain(specialty);

		vet.setFirstName(firstName);
		vet.setLastName(lastName);
		vet.addSpecialty(specialty);
		vetRepository.save(vet);

		Vet updatedVet = vetRepository.findById(vet.getId());

		assertThat(updatedVet.getFirstName()).isEqualTo(firstName);
		assertThat(updatedVet.getLastName()).isEqualTo(lastName);
		assertThat(updatedVet.getSpecialties()).contains(specialty);
	}

	@Test
	void shouldCreateVetWithNewSpeciality() {
		String firstName = "Miguel";
		String lastName = "Rodriguez";
		String specialtyName = "newTestSpeciality";

		Specialty specialty = new Specialty();
		specialty.setName(specialtyName);

		Vet vet = new Vet();
		vet.setFirstName(firstName);
		vet.setLastName(lastName);
		vet.addSpecialty(specialty);

		vetService.addVetWithSpecialty(vet);

		Specialty createdSpecialty = specialtyRepository.findById(specialty.getId());
		Vet createdVet = vetRepository.findById(vet.getId());
		assertThat(createdSpecialty).isNotNull();
		assertThat(createdVet).isNotNull();
		assertThat(createdVet.getFirstName()).isEqualTo(firstName);
		assertThat(createdVet.getLastName()).isEqualTo(lastName);
		assertThat(createdVet.getSpecialties()).contains(createdSpecialty);
	}

	@Test
	void shouldRemoveSpecialtyFromVet() {
		Vet vet = vetRepository.findById(3);
		int nrOfSpecialties = vet.getNrOfSpecialties();
		assertThat(nrOfSpecialties).isNotEqualTo(0);
		Integer specialtyId = vet.getSpecialties().get(0).getId();
		Specialty specialty = specialtyRepository.findById(specialtyId);
		assertThat(specialty).isNotNull();

		vet.removeSpecialty(specialty);
		vetRepository.save(vet);

		Vet createdVet = vetRepository.findById(vet.getId());
		assertThat(createdVet).isNotNull();
		assertThat(createdVet.getNrOfSpecialties()).isEqualTo(nrOfSpecialties - 1);
		assertThat(createdVet.getSpecialties()).doesNotContain(specialty);
	}

	@Test
	void shouldRemoveVet() {
		Vet vet = vetRepository.findById(1);
		assertThat(vet).isNotNull();
		List<Vet> all = (List<Vet>) vetRepository.findAll();
		assertThat(all).isNotEmpty();

		vetRepository.delete(vet);

		Collection<Vet> allAfterRemoval = vetRepository.findAll();
		assertThat(allAfterRemoval).isNotEmpty();

		assertThat(all.size() - 1).isEqualTo(allAfterRemoval.size());
		all.removeAll(allAfterRemoval);
		assertThat(all.size()).isEqualTo(1);
		assertThat(all.get(0)).isEqualTo(vet);
	}

	@Test
	void shouldDeleteSpecialtyAndRemoveThemFromVets() {
		int OGSize = specialtyRepository.findAll().size();
		String specialtyName;

		Specialty specialty = vetRepository.findById(3).getSpecialties().get(0);
		assertThat(specialty).isNotNull();
		assertThat(specialtyRepository.findAll()).contains(specialty);
		specialtyName = specialty.getName();
		specialtyService.deleteSpecialty(specialty);

		assertThat(specialtyRepository.existsById(specialty.getId())).isFalse();
		assertThat(specialtyRepository.findAll().size()).isEqualTo(OGSize - 1);
		assertThat(specialtyRepository.findAll().stream().noneMatch(s -> s.getName().equals(specialtyName))).isTrue();
		assertThat(vetRepository.findBySpecialtiesContaining(specialty)).isEmpty();
	}

	@Test
	void shouldFindBySpecialtyName() {
		String specialtyName;

		Specialty specialty = specialtyRepository.findById(1);
		assertThat(specialty).isNotNull();
		specialtyName = specialty.getName();
		List<Vet> foundSpecialties = vetRepository.findBySpecialty(specialtyName);

		assertThat(foundSpecialties).isNotEmpty();
		assertThat(foundSpecialties.stream().allMatch(vet -> vet.getSpecialties().contains(specialty))).isTrue();
		assertThat(foundSpecialties.size()).isNotEqualTo(0);
	}

	@Test
	void shouldFindBySpecialty() {
		Specialty specialty = specialtyRepository.findById(2);
		assertThat(specialty).isNotNull();
		List<Vet> foundSpecialties = vetRepository.findBySpecialtiesContaining(specialty);

		assertThat(foundSpecialties).isNotEmpty();
		assertThat(foundSpecialties.stream().allMatch(vet -> vet.getSpecialties().contains(specialty))).isTrue();
		assertThat(foundSpecialties.size()).isNotEqualTo(0);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/vet-constructor.csv", numLinesToSkip = 1)
	void testVetConstructor_fromFile(String firstName, String lastName, boolean valid) {
		Vet vet;
		try {
			vet = new Vet(firstName, lastName);
		}
		catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(vet).isNotNull();
		assertThat(vet.getFirstName()).isEqualTo(firstName);
		assertThat(vet.getLastName()).isEqualTo(lastName);
	}

}
