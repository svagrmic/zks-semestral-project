package org.springframework.samples.petclinic.zks;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.SpecialtyRepository;
import org.springframework.samples.petclinic.vet.SpecialtyService;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.stereotype.Service;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
// Ensure that if the mysql profile is active we connect to the real database:
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SpecialtyTests {

	@Autowired
	protected SpecialtyRepository specialtyRepository;

	@Autowired
	protected SpecialtyService specialtyService;

	@Autowired
	protected VetRepository vetRepository;

	@Test
	void shouldAddNewSpecialty() {
		int OGSize = specialtyRepository.findAll().size();
		String specialtyName = "newTestSpecialty";

		Specialty specialty = new Specialty();
		specialty.setName(specialtyName);
		assertThat(specialtyRepository.findAll().stream().noneMatch(s -> s.getName().equals(specialtyName))).isTrue();
		specialtyRepository.save(specialty);

		Specialty createdSpecialty = specialtyRepository.findById(specialty.getId());

		assertThat(specialtyRepository.findAll().size()).isEqualTo(OGSize + 1);
		assertThat(createdSpecialty).isNotNull();
		assertThat(createdSpecialty).isEqualTo(specialty);
		assertThat(createdSpecialty.getName()).isEqualTo(specialtyName);
	}

	@Test
	void shouldDeleteSpecialty() {
		int OGSize = specialtyRepository.findAll().size();
		String specialtyName;

		Specialty specialty = specialtyRepository.findById(1);
		assertThat(specialty).isNotNull();
		assertThat(specialtyRepository.findAll()).contains(specialty);
		specialtyName = specialty.getName();
		specialtyRepository.delete(specialty);

		assertThat(specialtyRepository.existsById(specialty.getId())).isFalse();
		assertThat(specialtyRepository.findAll().size()).isEqualTo(OGSize - 1);
		assertThat(specialtyRepository.findAll().stream().noneMatch(s -> s.getName().equals(specialtyName))).isTrue();
	}

	@Test
	void shouldDeleteSpecialtyAndRemoveThemFromVets() {
		int OGSize = specialtyRepository.findAll().size();
		String specialtyName;

		Specialty specialty = specialtyRepository.findById(1);
		assertThat(specialty).isNotNull();
		assertThat(specialtyRepository.findAll()).contains(specialty);
		specialtyName = specialty.getName();
		specialtyService.deleteSpecialty(specialty);

		assertThat(specialtyRepository.existsById(specialty.getId())).isFalse();
		assertThat(specialtyRepository.findAll().size()).isEqualTo(OGSize - 1);
		assertThat(specialtyRepository.findAll().stream().noneMatch(s -> s.getName().equals(specialtyName))).isTrue();
		assertThat(vetRepository.findAll().stream().noneMatch(vet -> vet.getSpecialties().contains(specialty)))
				.isTrue();
	}

	@Test
	void shouldUpdateSpecialty() {
		int OGSize = specialtyRepository.findAll().size();
		String specialtyName = "newTestSpecialty";

		Specialty specialty = specialtyRepository.findById(1);
		assertThat(specialty).isNotNull();
		specialty.setName(specialtyName);
		assertThat(specialtyRepository.findAll().stream().anyMatch(s -> s.getName().equals(specialtyName))).isTrue();
		specialtyRepository.save(specialty);

		Specialty createdSpecialty = specialtyRepository.findById(specialty.getId());

		assertThat(specialtyRepository.findAll().size()).isEqualTo(OGSize);
		assertThat(createdSpecialty).isNotNull();
		assertThat(createdSpecialty).isEqualTo(specialty);
		assertThat(createdSpecialty.getName()).isEqualTo(specialtyName);
	}

}
