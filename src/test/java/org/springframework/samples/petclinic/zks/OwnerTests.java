package org.springframework.samples.petclinic.zks;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.stereotype.Service;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
// Ensure that if the mysql profile is active we connect to the real database:
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class OwnerTests {

	@Autowired
	protected OwnerRepository ownerRepository;

	@ParameterizedTest
	@CsvFileSource(resources = "/owner-telephone.csv", numLinesToSkip = 1)
	public void testOwnerSetTelephone_fromFile(String telephone, boolean valid) {
		Owner owner = ownerRepository.findById(1);
		assertThat(owner).isNotNull();
		assertThat(owner.getTelephone()).isNotNull();
		String OGTelephone = owner.getTelephone();
		try {
			owner.setTelephone(telephone);
		}
		catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			assertThat(owner.getTelephone()).isEqualTo(OGTelephone);
			return;
		}
		assertThat(valid).isTrue();
		assertThat(owner.getTelephone()).isEqualTo(telephone);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/owner-city.csv", numLinesToSkip = 1)
	public void testOwnerSetCity_fromFile(String city, boolean valid) {
		Owner owner = ownerRepository.findById(1);
		assertThat(owner).isNotNull();
		assertThat(owner.getCity()).isNotNull();
		String OGCity = owner.getCity();
		try {
			owner.setCity(city);
		}
		catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			assertThat(owner.getCity()).isEqualTo(OGCity);
			return;
		}
		assertThat(valid).isTrue();
		assertThat(owner.getCity()).isEqualTo(city);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/owner-constructor.csv", numLinesToSkip = 1)
	public void testOwnerConstructor_fromFile(String address, String city, String telephone, String firstName,
			String lastName, boolean valid) {
		Owner owner;
		try {
			owner = new Owner(address, city, telephone, firstName, lastName);
		}
		catch (IllegalArgumentException e) {
			assertThat(valid).isFalse();
			return;
		}
		assertThat(valid).isTrue();
		assertThat(owner).isNotNull();
		assertThat(owner.getAddress()).isEqualTo(address);
		assertThat(owner.getCity()).isEqualTo(city);
		assertThat(owner.getTelephone()).isEqualTo(telephone);
		assertThat(owner.getFirstName()).isEqualTo(firstName);
		assertThat(owner.getLastName()).isEqualTo(lastName);
	}

}
