package org.springframework.samples.petclinic.zks;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Pageable;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.PetService;
import org.springframework.samples.petclinic.owner.PetTypeRepository;
import org.springframework.samples.petclinic.vet.SpecialtyRepository;
import org.springframework.samples.petclinic.vet.SpecialtyService;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.vet.VetService;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
// Ensure that if the mysql profile is active we connect to the real database:
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class VisitTests {

	@Autowired
	protected VisitRepository visitRepository;

	@Autowired
	protected PetRepository petRepository;

	@Test
	void shouldAddVisitToPet() {
		Integer petID = 1;
		String description = "Testing visit adding";
		LocalDate date = LocalDate.parse("2022-10-20");
		assertThat(petRepository.findById(petID)).isNotNull();
		Visit visit = new Visit();
		int OGMyPetVisits = visitRepository.findByPetId(petID).size();
		int OGAllVisits = visitRepository.findAll().size();

		visit.setDate(date);
		visit.setDescription(description);
		visit.setPetId(petID);

		visitRepository.save(visit);

		Collection<Visit> MyPetVisitsNew = visitRepository.findByPetId(petID);
		assertThat(MyPetVisitsNew).hasSize(OGMyPetVisits + 1);
		Collection<Visit> AllVisitsNew = visitRepository.findAll();
		assertThat(AllVisitsNew).hasSize(OGAllVisits + 1);

		Visit addedVisit = visitRepository.findById(visit.getId());
		assertThat(addedVisit).isNotNull();
		assertThat(addedVisit.getDate()).isEqualTo(date);
		assertThat(addedVisit.getPetId()).isEqualTo(visit.getPetId());
		assertThat(addedVisit.getDescription()).isEqualTo(description);
	}

}
