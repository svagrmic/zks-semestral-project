package org.springframework.samples.petclinic.zks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.samples.petclinic.owner.*;
import org.springframework.samples.petclinic.service.EntityUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
// Ensure that if the mysql profile is active we connect to the real database:
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PetTests {

	@Autowired
	protected PetRepository petRepository;

	@Autowired
	protected PetTypeRepository petTypeRepository;

	@Autowired
	protected PetService petService;

	@Test
	void shouldFindAllPetTypes() {
		List<PetType> petTypes = petTypeRepository.findAll();
		assertThat(petTypes.size()).isEqualTo(6);
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("cat"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("dog"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("turtle"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("hamster"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("rabbit"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("parrot"))).isTrue();
	}

	@Test
	void shouldFindAllPetTypesFromPets() {
		List<PetType> petTypes = petRepository.findPetTypes();
		assertThat(petTypes.size()).isEqualTo(6);
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("cat"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("dog"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("turtle"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("hamster"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("rabbit"))).isTrue();
		assertThat(petTypes.stream().anyMatch(petType -> petType.getName().equals("parrot"))).isTrue();
	}

	@Test
	void shouldNotAllowToAddPetTypeWithoutName() {
		PetType petType = new PetType();
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> petService.addPetType(petType));
	}

	@Test
	void shouldAddPetType() {
		String typeName = "testType";
		PetType petType = new PetType();
		int OGSize = petTypeRepository.findAll().size();
		assertThat(petTypeRepository.findByName(typeName)).isNull();

		petType.setName(typeName);
		petService.addPetType(petType);

		assertThat(petTypeRepository.findByName(typeName)).isNotNull();
		assertThat(petTypeRepository.findAll().size()).isEqualTo(OGSize + 1);
		assertThat(petTypeRepository.findById(petType.getId())).isNotNull();
		assertThat(petTypeRepository.findById(petType.getId()).getName()).isEqualTo(typeName);
	}

}
