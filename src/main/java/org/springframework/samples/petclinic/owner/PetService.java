package org.springframework.samples.petclinic.owner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PetService {

	@Autowired
	protected PetRepository petRepository;

	@Autowired
	protected PetTypeRepository petTypeRepository;

	public void addPetType(PetType petType) {
		List<PetType> petTypes = petRepository.findPetTypes();
		for (PetType pt : petTypes) {
			if (pt.getName().equals(petType.getName()))
				return;
		}
		petTypeRepository.save(petType);

	}

}
