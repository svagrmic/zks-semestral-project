package org.springframework.samples.petclinic.vet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SpecialtyService {

	@Autowired
	protected VetRepository vets;

	@Autowired
	protected SpecialtyRepository specialities;

	public void deleteSpecialty(Specialty specialty) {
		Collection<Vet> allVets = vets.findAll();
		for (Vet v : allVets) {
			if (v.getSpecialties().contains(specialty)) {
				v.removeSpecialty(specialty);
				vets.save(v);
			}
		}
		specialities.delete(specialty);
	}

}
